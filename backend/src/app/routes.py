from flask import jsonify, send_file, Blueprint, request, abort
from flask_cors import cross_origin

from src.utils.logging_config import app_log
from src.data.persistence_service import PersistenceService

main = Blueprint('main', __name__, url_prefix='/api/v1')


@main.route("/healthcheck")
# @cross_origin()
def test_log():
    app_log.debug("user called test logs function")
    return jsonify('test-logs called'), 200


# @cross_origin()
@main.route("/get-items/<checklist_id>", methods=['GET'])
def get_remaining_items(checklist_id):
    app_log.debug(f"attempting to load items for checklist with id '{checklist_id}'")

    persistence_service = PersistenceService()
    items = persistence_service.get_items(checklist_id)
    persistence_service.teardown()

    output = jsonify(items)
    app_log.debug(f"items for checklist with id '{checklist_id}': {output}")
    return output, 200


# @cross_origin()
@main.route("/set-items/<checklist_id>", methods=['POST'])
def set_remaining_items(checklist_id):
    app_log.debug(f"attempting to set items for checklist with id '{checklist_id}'")
    item_list = request.get_json()['items']
    app_log.debug(f"setting items to {item_list}")

    persistenceService = PersistenceService()
    persistenceService.set_items(checklist_id, item_list)
    persistenceService.teardown()

    return jsonify(''), 200
