import React, { Component } from 'react';
import Cookie from 'universal-cookie';
import Container from 'react-bootstrap/Container';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Badge from 'react-bootstrap/Badge';

class Navigation extends Component {


    render() {
        
        return (
            <Navbar collapseOnSelect expand="lg" className="shadow-sm bg-body-tertiary">
              <Container>
                <Navbar.Brand href="">{this.props.title}</Navbar.Brand>
              </Container>
            </Navbar>
        );
    }

}

export default Navigation;
