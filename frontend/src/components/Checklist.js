import React, { Component } from 'react';
import Cookie from 'universal-cookie';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css'
import Card from 'react-bootstrap/Card';
import Navigation from './Navigation';
import { ButtonGroup } from 'react-bootstrap';
import ListGroup from 'react-bootstrap/ListGroup';


class Checklist extends Component {

    constructor(props) {
        super(props);
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.handleOnSubmitAll = this.handleOnSubmitAll.bind(this);
        this.state = {
            checkedItems: [],
        };
    }

    handleOnChange(e) {
        console.log('handleOnChange: ', e.target.value);
        this.setState((prevState) => ({
            checkedItems: [...prevState.checkedItems, e.target.value],
        }));
    }

    handleOnSubmit(e) {
        e.preventDefault()
        console.log('checkedItems: ', this.state.checkedItems)
        const remainingItems = this.props.items.filter(item => !this.state.checkedItems.includes(item));
        this.props.onSubmit(remainingItems)
    }

    handleOnSubmitAll(e) {
        e.preventDefault()
        const remainingItems = []
        this.props.onSubmit(remainingItems)
    }

    render() {
        const items = this.props.items

        if (items == undefined || items.length == 0) {
            return null;
        }
    
        return (
            // <Form onSubmit={this.handleOnSubmit}>
            <Form >
                <Card className='p-2 shadow'>

                    <ListGroup variant="flush">
                    {items.map((item, index) => (
                        <ListGroup.Item>
                            <Form.Check 
                                defaultChecked={false}
                                className="text-nowrap" 
                                value={item} 
                                label={item} 
                                onChange={this.handleOnChange}
                            />
                        </ListGroup.Item>
                    ))}
                    </ListGroup>

                    <ButtonGroup className='shadow-sm bg-white rounded border-1 mt-2'>
                        <Button className='btn btn-light' type="submit" onClick={this.handleOnSubmitAll}>
                            Submit All
                        </Button>
                        <Button className='btn btn-light' variant='secondary' type="submit" onClick={this.handleOnSubmit}>
                            Submit Selected
                        </Button>
                    </ButtonGroup>
                </Card>
            </Form>
        );
      }
    }

export default Checklist;
