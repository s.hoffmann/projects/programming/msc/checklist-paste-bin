import React, { Component } from 'react';
import Cookie from 'universal-cookie';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css'
import Card from 'react-bootstrap/Card';
import Navigation from './Navigation';
import Checklist from './Checklist';


class ItemInput extends Component {

    constructor(props) {
        super(props);
        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.state = {
            input: '',
        };
    }

    handleInputChange(e) {
        console.log(e.target.value)
        this.setState({ input: e.target.value })
    }

    handleOnSubmit(e) {
        e.preventDefault()
        console.log('handle submit: ', this.state.input)
        const output = this.state.input
        this.setState({ input: '' })
        this.props.onSubmit(output)
    }

    render() {

        return (
            <div>
                <Card className='my-3 p-2 shadow bg-white rounded border-1'>

                    <Form onSubmit={this.handleOnSubmit}>
                        <Form.Group className="d-flex">
                            <Form.Control 
                                type="text" 
                                placeholder="New item" 
                                value={this.input}
                                onChange={this.handleInputChange}
                            />
                            <div className='minimal-width-div' />
                            <Button className='ml-3 btn btn-light' type="submit">Submit</Button>
                        </Form.Group>
                    </Form>

                    <Checklist 
                        items={this.state.remainingItems}
                        onSubmit={this.handleOnSubmit}
                    />
                </Card>
            </div>
        );
    }
}

export default ItemInput;
