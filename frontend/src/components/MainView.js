import { BrowserRouter, Routes, Route, useParams } from 'react-router-dom';
import React, { Component } from 'react';
import Cookie from 'universal-cookie';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css'
import Card from 'react-bootstrap/Card';
import Navigation from './Navigation';
import Checklist from './Checklist';
import ItemInput from './ItemInput';
import { setItems, getItems } from '../logic/ApiFetcher';


class MainView extends Component {

    constructor(props) {
        super(props);
        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        this.handleOnInput = this.handleOnInput.bind(this)
	    console.log('main View title: ', this.props.title)
        this.state = {
            remainingItems: [],
        };
    }

    componentDidMount() {
        const setStateCallback = remainingItems => this.setState({ remainingItems })
        getItems(setStateCallback, this.props.title)
    }

    handleOnSubmit(input) {
        console.log('remainingItems: ', input)
        this.setState({ remainingItems: input })
        console.log('parent handle: ', this.state.remainingItems)

        setItems(input, this.props.title)
    }

    handleOnInput(newItem) {
        if (newItem == null || newItem.length == 0) {
            console.log('input empty - submitting nothing')
            return;
        }
        console.log('new item: ', newItem)
        this.state.remainingItems.push(newItem)
        console.log('new state: ', this.state.remainingItems)

        setItems(this.state.remainingItems, this.props.title)
    }

    render() {

        return (
            <div>
                <Navigation onLogout={this.props.onLogout} title={this.props.title} />

                <Container className='mt-2'>
                    <ItemInput onSubmit={this.handleOnInput} />

                    <Checklist 
                        items={this.state.remainingItems}
                        onSubmit={this.handleOnSubmit}
                    />
                </Container>
            </div>
        );
    }
}

export default MainView;
