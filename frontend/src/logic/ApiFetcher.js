// const SERVER_ADDRESS = 'http://sh-dev.dns.army:8081'
const SERVER_ADDRESS = 'http://192.168.178.172:8081'
export const SERVER_BASE = SERVER_ADDRESS + '/api/v1'


export function setItems(items, checklistTitle) {
    const url = SERVER_BASE + '/set-items/' + checklistTitle;
        console.log('handle on submit url: ', url)
        const data = {
            items: items,
        };

        // Create the request headers
        const headers = new Headers({
            'Content-Type': 'application/json',
        // You can add other headers as needed
        });

        // Create the request object
        const requestOptions = {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(data), // Convert data to JSON string
        };

        // Make the POST request
        fetch(url, requestOptions)
        .then(response => {
            if (!response.ok) {
            throw new Error('Network response was not ok');
            }
            return response.json(); // Parse the response body as JSON
        })
        .then(data => {
            console.log('Success:', data);
            window.location.reload();
            // You can work with the JSON response here
        })
        .catch(error => {
            console.error('Error:', error);
        });
}

export function getItems(setStateCallback, checklistTitle) {
    fetch(SERVER_BASE + '/get-items/' + checklistTitle)
        .then(response => response.json())
        .then(setStateCallback)
        .catch(error => console.error(error));
}


