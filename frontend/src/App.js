import { BrowserRouter, Routes, Route, useParams } from 'react-router-dom';
import MainView from './components/MainView';
import './App.css';

function App() {
  const { param } = useParams()
  console.log('param app: ', param)

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/:param" element={<IndexComponent />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;

function IndexComponent() {
  const { param } = useParams();
  console.log('index param: ', param)

  return (
    <div>
      <MainView title={param} />
    </div>
  );
}


