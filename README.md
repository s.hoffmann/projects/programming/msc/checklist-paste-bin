# checklist-paste-bin

Create checklists to share with people without any user authentication. Anyone who has the given link can access the content. To create a new checklist simply set the title in the following way: `[SERVER_BASE][SERVER_PORT]/[TITLE]`

If the title is already used by another user, the url connects to their checklist. Otherwise it creates a new checklist instance.


## Live Server
To test the application please visit [this](http://sh-dev.dns.army:8080/dummy-checklist) server.


## User Interface

<img src="_miscellaneous/screenshots/screenshot1.jpeg" alt="Image Description" width="400">


## Technology Stack
- React.js 18.2.0
- Flask 3.0.0
- bootstrap 5.3.2
- sqlite 3.37.2

## Dev Commands
```
# run backend in debug mode
flask --app main.py run --reload --debug --host=0.0.0.0
```

```bash
psql -d wotd_db -U admin
```

